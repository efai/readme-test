# About TRUST

TRUST is a very universal testing setUp for Laravel with routes, store and update tests. TRUST stands for TestCase, Routes, Updates, Stores, Traits

---

# Integration

You can integrate TRUST in new or an ongoing project with no problem

---

# Create a new project

Create a new project in PHP > 7.3.0

    composer new <name_of_the_project>

Create a new project in PHP < 7.3.0

    composer create-project laravel/laravel <name_of_the_project>

---

# Download package

Download package from bitbucket and add dependencies into your composer.json in your project (pay attention to "url" parameter and change the location if needed):

    "repositories": {
        "dev-package": {
            "type": "path",
            "url": "/laragon/packages/tests/trust-test-package",
            "options": {
                "symlink": true
            }
        }
    }

---

# Installation

Install composer

    composer install

Get package into your vendor folder

    composer require marvic/trust

Run a console command that copies all the necessary files to your project

    php artisan trust:install

Delete the extension '.stub' from new copied files [*--required*]

- 'tests' folder
    - DataSeeder.php.stub
    - TestCase.php.stub

- 'tests\Feature\Trust' folder
    - RelationshipsTest.php.stub

Delete dependencies [*--required_if*] - when you want to share your project via git

- delete all trust dependencies from composer.json and composer.lock

---

# Basic Usage

Everything starts with TestCase.php file. There you have to set a few things

- namespace
    - it points to your Models in your applications

- models
    - there's an array of all your models, which do you want to send through trust
    - each model then will be tests against the following rules
        - routes
            - routes for each model are defined in web.php (index, create, store, edit, update)
            - routes for each model are named like <model_name>.index, <model_name>.create, <model_name>.store, <model_name>.edit and <model_name>.update in web.php
            - controller have all routes methods defined (index, create, store, edit, update)
            - routes index, create and edit doesn't have redirect in it
            - routes store and update does have redirect in it
        - store/update (for both feature and unit tests)
            - DB is properly set
            - store and update method have the same count of columns to fill and doesn't contained any extra data
            - model have filled mass assignment (protected $fillable/protected $guarded)
            - in DataSeeder.php are set all seeding columns with fake/mock data
        - relationships [*--optional*] - as default, there's one check already written for test. It's a role_user relationship
            - DB is properly set
            - model have filled mass assignment (protected $fillable/protected $guarded)
            - in case of 1:1 or 1:N
                - single relationship have properly set relation
            - in case of N:N
                - both models have relationship between each other
                - pivot tables follow convention of alphabetically created table (alpha_beta)

- setUp [*--optional*]
    - there's an option for less/more verbal errors, just enable/disable 'withoutExceptionHandling' method

DataSeeder.php have to be filled with data for DB columns via customed faker. There's a list with possible options:

- generateName(int $length = 255): string
- generateWord(int $length = 255): string
- generateNumberBetween(int $min, int $max): int
- generateFloat(int $nbMaxDecimals = 2, int $min = 0, int $max = 7): float
- generateCatchPhrase(): string

# License

The TRUST is open-source software licensed under the [MIT license] (https://opensource.org/licenses/MIT)
